﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBeahviour : MonoBehaviour {
	public Rigidbody2D rb;
	public Vector3 rotationValue;
	public Animator anim;
    public GameObject resetPos;

	public string animName;

	public float gravityScaleValue;

	public bool stick;

	public float speed = 3;
    public float timer;
    public float changeSpeed;

	Transform camera;

	bool lockMove;
    public bool facingRight;
    public bool go = false;
    public bool play = false;

    // Start is called before the first frame update
    void Start() {
		rb = this.GetComponent<Rigidbody2D>();
		anim = this.GetComponent<Animator>();
		camera = GetComponentInChildren<Camera>().transform;
	}

	// Update is called once per frame
	void FixedUpdate() {
        if(play)
        {
            float horizontalMovement = Input.GetAxis("Horizontal");
            if (horizontalMovement != 0)
            {
                if (horizontalMovement > 0)
                {
                    if (facingRight) Flip();
                    this.transform.Translate(Vector2.right * speed * Time.deltaTime);
                }
                if (horizontalMovement < 0)
                {
                    if (!facingRight) Flip();
                    this.transform.Translate(Vector2.left * speed * Time.deltaTime);
                }
                anim.SetBool("Move", true);
            }
            else
            {
                anim.SetBool("Move", false);
            }

            if (Input.GetKey(KeyCode.C))
            {
                rb.velocity = new Vector2(rb.velocity.x, 1);
            }

            if(Input.GetKey(KeyCode.Space))
            {
                transform.position = Vector3.MoveTowards(transform.position, resetPos.transform.position, 50000 * Time.deltaTime);
            }
        }
        else if (go)
        {
            this.transform.Translate(Vector2.right * Time.deltaTime / 2);
        }
	}

	private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.tag == "StickyCeil") {
			stick = true;
			rotationValue = new Vector3(180, 0, 0);
		} else if (collision.gameObject.tag == "StickyWallLeft") {
			Debug.Log("called StickyWallLeft");
			stick = true;
			rb.gravityScale = gravityScaleValue;
			rotationValue = new Vector3(0, 0, -90);
			animName = "StickVertical";
		} else if (collision.gameObject.tag == "StickyWallRight") {
			Debug.Log("called StickyWallRight");
			stick = false;
			rb.gravityScale = gravityScaleValue;
			rotationValue = new Vector3(0, 0, 90);
			animName = "StickVertical";
		}
        else if(collision.gameObject.tag == "Mushroom")
        {
            anim.SetBool("Roll", true);
        }
	}

	private void OnTriggerExit2D(Collider2D collision) {
		if (collision.gameObject.tag == "StickyCeil"
			|| collision.gameObject.tag == "StickyWallRight"
			|| collision.gameObject.tag == "StickyWallLeft") {
			stick = false;
			rb.gravityScale = gravityScaleValue;
			rotationValue = new Vector3(0, 0, 0);
		}
        else if(collision.gameObject.tag == "Floor" && rb.velocity.y < 0.3f && rb.velocity.y > -0.3f)
        {
            anim.SetBool("Roll", false);
        }
	}

	public void LockMove() {
		lockMove = true;
		StartCoroutine(UnlockMove());
	}
	IEnumerator UnlockMove() {
		yield return new WaitForSeconds(1);
		lockMove = false;
	}

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
