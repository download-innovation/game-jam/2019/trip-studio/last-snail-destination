﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBeahviour : MonoBehaviour {
	public Rigidbody2D rb;
	public Vector3 rotationValue;
	public Animator anim;

	public string animName;

	public float gravityScaleValue;

	public bool stick;

	public float speed = 3;

	Transform camera;

	bool lockMove;

	// Start is called before the first frame update
	void Start() {
		rb = this.GetComponent<Rigidbody2D>();
		anim = this.GetComponent<Animator>();
		camera = GetComponentInChildren<Camera>().transform;
	}

	// Update is called once per frame
	void FixedUpdate() {
		float horizontalMovement = Input.GetAxis("Horizontal");

		//#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.U)) {
			transform.position = transform.position + new Vector3(0, 22 * Time.deltaTime, 0);
		}
		//#endif

		if (horizontalMovement != 0 && !lockMove) {
			rb.velocity = new Vector2(horizontalMovement * speed, rb.velocity.y);
			//} else {
			//	rb.velocity = new Vector2(0, rb.velocity.y);
		}
		if (stick && Input.GetKeyDown(KeyCode.C)) {
			rb.gravityScale = 0;
			rb.velocity = new Vector2(rb.velocity.x, 0);
			this.transform.rotation = new Quaternion(rotationValue.x,
													 rotationValue.y,
													 rotationValue.z,
													 0);
			if (animName != null) {
				//anim.SetTrigger(animName);
				Debug.Log("Doing the animation: " + animName);
			}
		} else if (Input.GetKeyUp(KeyCode.C)) {
			rb.gravityScale = gravityScaleValue;
			this.transform.rotation = new Quaternion(0, 0, 0, 0);
			animName = null;
		}

		/*if (camera.position.y < previousCameraPosition.y) {
			camera.position = new Vector3(camera.position.x, previousCameraPosition.y, camera.position.z);
		} else if (camera.localPosition.y > 0) {
			camera.position = new Vector3(camera.position.x, previousCameraPosition.y, camera.position.z);
		}*/
	}

	private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.tag == "StickyCeil") {
			stick = true;
			rotationValue = new Vector3(180, 0, 0);
		} else if (collision.gameObject.tag == "StickyWallLeft") {
			Debug.Log("called StickyWallLeft");
			stick = true;
			rb.gravityScale = gravityScaleValue;
			rotationValue = new Vector3(0, 0, -90);
			animName = "StickVertical";
		} else if (collision.gameObject.tag == "StickyWallRight") {
			Debug.Log("called StickyWallRight");
			stick = false;
			rb.gravityScale = gravityScaleValue;
			rotationValue = new Vector3(0, 0, 90);
			animName = "StickVertical";
		}
	}

	private void OnTriggerExit2D(Collider2D collision) {
		if (collision.gameObject.tag == "StickyCeil"
			|| collision.gameObject.tag == "StickyWallRight"
			|| collision.gameObject.tag == "StickyWallLeft") {
			stick = false;
			rb.gravityScale = gravityScaleValue;
			rotationValue = new Vector3(0, 0, 0);
		}
	}

	public void LockMove() {
		lockMove = true;
		StartCoroutine(UnlockMove());
	}
	IEnumerator UnlockMove() {
		yield return new WaitForSeconds(1);
		lockMove = false;
	}
}
