﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level1Manager : MonoBehaviour
{
    public GameObject player;
    public GameObject go;
    public GameObject vignetta;
    public PlayerBeahviour pB;
    public GameObject stagaDet;
    public GameObject oh;
    public GameObject sun;
    public Camera camera1;
    public Camera camera2;
    public float timer;
    public int times;
    public bool switchCamera;
    public bool loadScene;

    // Start is called before the first frame update
    void Start()
    {
        pB = player.GetComponent<PlayerBeahviour>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= 8f && times < 1)
        {
            go.SetActive(false);
            pB.go = true;
            vignetta.SetActive(true);
            times++;
            timer = 0;
        }
        if(timer > 12 && times == 1)
        {

            vignetta.SetActive(false);
            stagaDet.SetActive(true);
            pB.go = false;
            times++;
            timer = 0;
        }
        if(timer > 1 && times == 2)
        {
            stagaDet.SetActive(false);
            oh.SetActive(true);
            times++;
            timer = 0;
        }
        if(timer > 1 && times == 3)
        {
            oh.SetActive(false);
            pB.play = true;
            times++;
        }
        if(switchCamera)
        {
            camera2.transform.position = Vector3.MoveTowards(camera2.transform.position, sun.transform.position, 5 * Time.deltaTime);
            if(times == 4)
            {
                timer = 0;
                loadScene = true;
                times++;
            }           
        }
        if (timer > 6 && loadScene)
        {
            SceneManager.LoadScene(0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            camera1.enabled = false;
            camera2.enabled = true;
            switchCamera = true;
        }
    }
}
