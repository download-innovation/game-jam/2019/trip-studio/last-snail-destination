﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReversePlayer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<SpriteRenderer>().flipY = true;
        }
    }

    private void OnTriggerExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<SpriteRenderer>().flipY = false;
        }
    }
}
