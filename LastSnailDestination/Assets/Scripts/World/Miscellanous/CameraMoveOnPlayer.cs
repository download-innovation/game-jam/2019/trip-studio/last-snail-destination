﻿using UnityEngine;
using System.Collections;

public class CameraMoveOnPlayer : MonoBehaviour
{

    public GameObject player;       //Public variable to store a reference to the player game object


    private Vector3 offset;         //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        player = GameObject.FindGameObjectWithTag("Player");
        offset = transform.position - player.transform.position;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        if(Mathf.Abs(transform.position.x - player.transform.position.x) > 5
           || Mathf.Abs(transform.position.y - player.transform.position.y) > 7)
        transform.position = Vector3.MoveTowards(transform.position,  player.transform.position + offset, 5 * Time.deltaTime);
    }
}