﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootMe : MonoBehaviour
{
    public float intensityOfExplosion;
    Vector2 angle;
    PolygonCollider2D box;
    // Start is called before the first frame update
    void Start()
    {
        box = PolygonCollider2D.FindObjectOfType<PolygonCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Collisione");
            //float vel = collision.relativeVelocity.magnitude;
            angle = new Vector2(collision.contacts[0].normal.x + .1f, collision.contacts[0].normal.y - .1f);
            //collision.gameObject.GetComponent<PMov>().Kabloom(4,4, -angle);
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(-angle * 500);
            this.gameObject.active = false;
        }
    }
}
