﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {
	public GameObject panel;

	private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.GetComponent<PlayerBeahviour>()) {
			panel.SetActive(true);
		}
	}
}
