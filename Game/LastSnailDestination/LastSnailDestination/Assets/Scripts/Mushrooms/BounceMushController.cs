﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceMushController : MonoBehaviour {
	public SpriteRenderer spriteRenderer;

	public float resizeSpeed;
	public float timer;
	public float stopTime;
	public float colorTimer;
	public float changeColorTime;
	public float pushForce = 15;
	public bool resize;
	public bool resized;
	public float fallForce = 10;
	public bool lockPlayerMove;
	public GameObject balloon;

	private void Start() {
		spriteRenderer = this.GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	void Update() {
		/*colorTimer += Time.deltaTime;
		if (colorTimer >= changeColorTime) {
			spriteRenderer.color = new Color(Random.value, Random.value, Random.value);
			colorTimer = 0;
		}*/


		if (resize) {
			timer += Time.deltaTime;
			this.transform.localScale = new Vector3(transform.localScale.x * resizeSpeed,
													transform.localScale.y * resizeSpeed,
													transform.localScale.z * resizeSpeed);
			if (timer >= stopTime) {
				resize = false;
			}
		}
	}

	private void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Player" /*&& !resized*/) {
			resize = true;
			resized = true;
			GetComponent<Rigidbody2D>().AddForce(-transform.up * fallForce);
			collision.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * pushForce, ForceMode2D.Impulse);
			if (lockPlayerMove)
				collision.gameObject.GetComponent<PlayerBeahviour>().LockMove();
			ShowBalloon();
		}
	}

	void ShowBalloon() {
		if (balloon) {
			balloon.SetActive(true);
			StartCoroutine(HideBalloon());
		}
	}
	IEnumerator HideBalloon() {
		yield return new WaitForSeconds(3);
		balloon.SetActive(false);
	}
}
