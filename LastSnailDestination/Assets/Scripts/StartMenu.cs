﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour {
	public Button start;
	public Button credits;
	public Button quit;
	[Space]
	public GameObject creditsPanel;
	public Button creditsPanelClose;

	// Start is called before the first frame update
	void Start() {
		start.onClick.AddListener(() => {
			SceneManager.LoadScene(1);
		});
		credits.onClick.AddListener(() => {
			gameObject.SetActive(false);
			creditsPanel.SetActive(true);
		});
		quit.onClick.AddListener(() => { Application.Quit(); });

		creditsPanelClose.onClick.AddListener(() => {
			gameObject.SetActive(true);
			creditsPanel.SetActive(false);
		});
	}

	// Update is called once per frame
	void Update() {

	}
}
