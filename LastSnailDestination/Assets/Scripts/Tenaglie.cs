﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tenaglie : MonoBehaviour {
	public Transform tenaglia1;
	public Transform tenaglia2;
	public float speed = 5;
	public float hiddenWait = 3;
	public float showedWait = 1;
	[Range(0, 100)]
	public float tenaglia1ShowX;
	[Range(-100, 0)]
	public float tenaglia2ShowX;
	float tenaglia1HiddenX;
	float tenaglia2HiddenX;

	void Start() {
		tenaglia1HiddenX = tenaglia1.localPosition.x;
		tenaglia2HiddenX = tenaglia2.localPosition.x;
		StartCoroutine(Coroutine());
	}

	IEnumerator Coroutine() {
		while (true) {
			yield return new WaitForSeconds(hiddenWait);
			while (tenaglia1.localPosition.x > tenaglia1ShowX) {
				tenaglia1.localPosition = tenaglia1.localPosition - new Vector3(tenaglia1ShowX * speed * Time.deltaTime, 0, 0);
				tenaglia2.localPosition = tenaglia2.localPosition - new Vector3(tenaglia2ShowX * speed * Time.deltaTime, 0, 0);
				yield return 0;
			}
			yield return new WaitForSeconds(showedWait);
			while (tenaglia1.localPosition.x < tenaglia1HiddenX) {
				tenaglia1.localPosition = tenaglia1.localPosition + new Vector3(tenaglia1HiddenX * speed * Time.deltaTime, 0, 0);
				tenaglia2.localPosition = tenaglia2.localPosition + new Vector3(tenaglia2HiddenX * speed * Time.deltaTime, 0, 0);
				yield return 0;
			}
		}
	}
}
